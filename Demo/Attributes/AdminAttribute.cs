﻿namespace Demo.Attributes
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = false)]
    public class AdminAttribute : Attribute
    {
    }
}
