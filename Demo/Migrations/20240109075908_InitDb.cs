﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Demo.Migrations
{
    /// <inheritdoc />
    public partial class InitDb : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "dm_user",
                columns: table => new
                {
                    mauser = table.Column<string>(name: "ma_user", type: "nvarchar(10)", maxLength: 10, nullable: false),
                    tenuser = table.Column<string>(name: "ten_user", type: "nvarchar(50)", maxLength: 50, nullable: false),
                    fullname = table.Column<string>(name: "full_name", type: "nvarchar(50)", maxLength: 50, nullable: false),
                    password = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: false),
                    manhom = table.Column<string>(name: "ma_nhom", type: "nvarchar(10)", maxLength: 10, nullable: false),
                    madonvi = table.Column<string>(name: "ma_donvi", type: "nvarchar(8)", maxLength: 8, nullable: false),
                    maphong = table.Column<string>(name: "ma_phong", type: "nvarchar(11)", maxLength: 11, nullable: false),
                    mabenhvien = table.Column<string>(name: "ma_benhvien", type: "nvarchar(20)", maxLength: 20, nullable: true),
                    phanquyen = table.Column<bool>(name: "phan_quyen", type: "bit", nullable: false),
                    nguoidungBLVP = table.Column<bool>(name: "nguoidung_BLVP", type: "bit", nullable: true),
                    qthtBLVP = table.Column<bool>(name: "qtht_BLVP", type: "bit", nullable: true),
                    matthai = table.Column<string>(name: "ma_tthai", type: "nvarchar(2)", maxLength: 2, nullable: false),
                    mactuarr = table.Column<string>(name: "ma_ctu_arr", type: "nvarchar(150)", maxLength: 150, nullable: false),
                    mapktarr = table.Column<string>(name: "ma_pkt_arr", type: "nvarchar(150)", maxLength: 150, nullable: false),
                    mapttaiarr = table.Column<string>(name: "ma_pttai_arr", type: "nvarchar(150)", maxLength: 150, nullable: false),
                    ngaycnhat = table.Column<DateTime>(name: "ngay_cnhat", type: "datetime2", nullable: true),
                    matthaittoan = table.Column<string>(name: "ma_tthai_ttoan", type: "nvarchar(8)", maxLength: 8, nullable: true),
                    trangthai = table.Column<bool>(name: "trang_thai", type: "bit", nullable: false),
                    email = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    machucvu = table.Column<string>(name: "ma_chucvu", type: "nvarchar(2)", maxLength: 2, nullable: false),
                    macbcnv = table.Column<string>(name: "ma_cbcnv", type: "nvarchar(11)", maxLength: 11, nullable: false),
                    makh = table.Column<string>(name: "ma_kh", type: "nvarchar(50)", maxLength: 50, nullable: false),
                    passwordsign = table.Column<string>(name: "password_sign", type: "nvarchar(50)", maxLength: 50, nullable: false),
                    passwordsha256 = table.Column<string>(name: "password_sha256", type: "nvarchar(250)", maxLength: 250, nullable: true),
                    OTPcode = table.Column<string>(name: "OTP_code", type: "nvarchar(6)", maxLength: 6, nullable: false),
                    OTPdisable = table.Column<bool>(name: "OTP_disable", type: "bit", nullable: false),
                    livetime = table.Column<DateTime>(name: "live_time", type: "datetime2", nullable: true),
                    livenum = table.Column<int>(name: "live_num", type: "int", nullable: false),
                    EmailPvi = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dm_user", x => x.mauser);
                });

            migrationBuilder.CreateTable(
                name: "dm_user_info",
                columns: table => new
                {
                    prkey = table.Column<int>(name: "pr_key", type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    mauser = table.Column<string>(name: "ma_user", type: "nvarchar(10)", maxLength: 10, nullable: false),
                    avatar = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    ngaysinh = table.Column<DateTime>(name: "ngay_sinh", type: "datetime2", nullable: false),
                    gioitinh = table.Column<byte>(name: "gioi_tinh", type: "tinyint", nullable: false),
                    sdt = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    strachoc = table.Column<bool>(name: "strac_hoc", type: "bit", nullable: false),
                    solanloginfail = table.Column<int>(name: "so_lan_login_fail", type: "int", nullable: false),
                    thoigiankhoa = table.Column<DateTime>(name: "thoi_gian_khoa", type: "datetime2", nullable: false),
                    isadmin = table.Column<bool>(name: "is_admin", type: "bit", nullable: false),
                    qrcode = table.Column<string>(name: "qr_code", type: "nvarchar(max)", nullable: true),
                    chucdanh = table.Column<string>(name: "chuc_danh", type: "nvarchar(max)", nullable: true),
                    tel = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    andanh = table.Column<bool>(name: "an_danh", type: "bit", nullable: false),
                    qraddress = table.Column<string>(name: "qr_address", type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dm_user_info", x => x.prkey);
                });

            migrationBuilder.CreateTable(
                name: "refresh_token",
                columns: table => new
                {
                    prkey = table.Column<int>(name: "pr_key", type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    mauser = table.Column<string>(name: "ma_user", type: "nvarchar(10)", maxLength: 10, nullable: false),
                    token = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    thoigiantao = table.Column<DateTime>(name: "thoi_gian_tao", type: "datetime2", nullable: false),
                    thoigianketthuc = table.Column<DateTime>(name: "thoi_gian_ket_thuc", type: "datetime2", nullable: false),
                    thuhoi = table.Column<bool>(name: "thu_hoi", type: "bit", nullable: false),
                    sudung = table.Column<bool>(name: "su_dung", type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_refresh_token", x => x.prkey);
                });

            migrationBuilder.CreateTable(
                name: "thiet_bi",
                columns: table => new
                {
                    prkey = table.Column<int>(name: "pr_key", type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    mauser = table.Column<string>(name: "ma_user", type: "nvarchar(10)", maxLength: 10, nullable: false),
                    tokenthietbi = table.Column<string>(name: "token_thiet_bi", type: "nvarchar(max)", nullable: false),
                    loaithietbi = table.Column<byte>(name: "loai_thiet_bi", type: "tinyint", nullable: false),
                    thoigianhethan = table.Column<DateTime>(name: "thoi_gian_het_han", type: "datetime2", nullable: false),
                    ngonngu = table.Column<string>(name: "ngon_ngu", type: "nvarchar(max)", nullable: false, defaultValue: "vi")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_thiet_bi", x => x.prkey);
                });

            migrationBuilder.CreateTable(
                name: "thong_bao",
                columns: table => new
                {
                    prkey = table.Column<int>(name: "pr_key", type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    mauser = table.Column<string>(name: "ma_user", type: "nvarchar(10)", maxLength: 10, nullable: false),
                    tieude = table.Column<string>(name: "tieu_de", type: "nvarchar(max)", nullable: false),
                    noidung = table.Column<string>(name: "noi_dung", type: "nvarchar(max)", nullable: false),
                    thoigiantao = table.Column<DateTime>(name: "thoi_gian_tao", type: "datetime2", nullable: false),
                    dadoc = table.Column<bool>(name: "da_doc", type: "bit", nullable: true),
                    loaithongbao = table.Column<byte>(name: "loai_thong_bao", type: "tinyint", nullable: false),
                    refid = table.Column<int>(name: "ref_id", type: "int", nullable: false),
                    thamso = table.Column<string>(name: "tham_so", type: "nvarchar(max)", nullable: false),
                    thamsochiso = table.Column<string>(name: "tham_so_chi_so", type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_thong_bao", x => x.prkey);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "dm_user");

            migrationBuilder.DropTable(
                name: "dm_user_info");

            migrationBuilder.DropTable(
                name: "refresh_token");

            migrationBuilder.DropTable(
                name: "thiet_bi");

            migrationBuilder.DropTable(
                name: "thong_bao");
        }
    }
}
