﻿using AutoMapper;
using Demo.Dtos.Notifications;
using Demo.Dtos.Users;
using Demo.Entities;

namespace Demo
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, UserDto>().ForMember(x => x.Role, opt => opt.Ignore());

            CreateMap<Notification, NotificationDto>();
        }
    }
}
