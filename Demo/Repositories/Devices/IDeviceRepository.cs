﻿using Demo.Entities;

namespace Demo.Repositories.Devices
{
    public interface IDeviceRepository : IEfCoreRepository<Device>
    {
    }
}
