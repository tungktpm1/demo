﻿using Demo.Entities;
using Demo.EntityFrameworkCore;

namespace Demo.Repositories.Devices
{
    public class DeviceRepository : EfCoreRepository<Device>, IDeviceRepository
    {
        public DeviceRepository(DemoDbContext pVIReSmartDbContext)
            : base(pVIReSmartDbContext)
        {
        }
    }
}
