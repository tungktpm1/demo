﻿using Demo.Dtos.Users;
using Demo.Entities;
using Demo.EntityFrameworkCore;
using Demo.Enums;
using Demo.Extensions;
using Deno.Dtos.Users;
using Microsoft.EntityFrameworkCore;

namespace Demo.Repositories.Users
{
    public class UserRepository : EfCoreRepository<User>, IUserRepository
    {
        private IConfiguration Configuration { get; }

        public UserRepository(DemoDbContext DemoDbContext, IConfiguration configuration)
            : base(DemoDbContext)
        {
            Configuration = configuration;
        }

        public async Task AddUserInfoAsync(UserInfo userInfo)
        {
            await DemoDbContext.UserInfos.AddAsync(userInfo);

            await DemoDbContext.SaveChangesAsync();
        }

        public async Task<List<UserQrCodeDto>> GetAllUserHasQrCodeAsync()
        {
            return await DbSet.Join(DemoDbContext.UserInfos, x => x.UserCode, p => p.UserCode, (x, p) => new UserQrCodeDto
            {
                FullName = x.FullName,
                QrCode = p.QrCode
            }).ToListAsync();
        }

        public async Task<List<UserInfo>> GetAllUserInfoAsync()
        {
            return await DemoDbContext.UserInfos.ToListAsync();
        }

        public async Task<List<User>> GetAllUsersSortByNameAsync()
        {
            return await DbSet.OrderBy(x => x.FullName).ToListAsync();
        }

        public async Task<UserInfo> GetCurrentUserInfoAsync(string userCode)
        {
            return await DemoDbContext.UserInfos.FirstOrDefaultAsync(x => x.UserCode == userCode);
        }

        public async Task<UserDto> GetUserByUserCodeAsync(string userCode)
        {
            var query = from x in DbSet.Where(x => x.UserCode == userCode)
                        join p in DemoDbContext.UserInfos on x.UserCode equals p.UserCode into gr
                        from p in gr.DefaultIfEmpty()
                        select new UserDto
                        {
                            Avatar = p != null ? p.Avatar : null,
                            UserCode = x.UserCode,
                            DateOfBirth = p != null ? p.DateOfBirth : default,
                            Decentralization = x.Decentralization,
                            DepartmentCode = x.DepartmentCode,
                            DivisionCode = x.DivisionCode,
                            Email = x.Email,
                            EmployeeCode = x.EmployeeCode,
                            FullName = x.FullName,
                            Gender = p != null ? p.Gender : Gender.Male,
                            GroupCode = x.GroupCode,
                            HospitalCode = x.HospitalCode,
                            IsActive = x.IsActive,
                            LiveNumber = x.LiveNumber,
                            LiveTime = x.LiveTime,
                            Name = x.Name,
                            PaymentStatusCode = x.PaymentStatusCode,
                            PhoneNumber = p != null ? p.PhoneNumber : null,
                            PositionCode = x.PositionCode,
                            QthtBlvp = x.QthtBlvp,
                            StatusCode = x.StatusCode,
                            UpdateDate = x.UpdateDate,
                            UseBiometric = p != null && p.UseBiometric,
                            UserBlvp = x.UserBlvp,
                            IsAdmin = p != null && p.IsAdmin,
                            QrCode = p != null ? p.QrCode : null,
                            Tel = p != null ? p.Tel : null,
                            QrAddress = p != null ? p.QrAddress : null,
                            Title = p != null ? p.Title : null,
                            IsAnonymous = p != null && p.IsAnonymous,
                            EmailPvi = x.EmailPvi
                        };

            return await query.FirstOrDefaultAsync();
        }

        public async Task<UserDto> GetUserByEmailAsync(string email)
        {
            var query = from x in DbSet.Where(x => x.EmailPvi == email)
                        join p in DemoDbContext.UserInfos on x.UserCode equals p.UserCode into gr
                        from p in gr.DefaultIfEmpty()
                        select new UserDto
                        {
                            Avatar = p != null ? p.Avatar : null,
                            UserCode = x.UserCode,
                            DateOfBirth = p != null ? p.DateOfBirth : default,
                            Decentralization = x.Decentralization,
                            DepartmentCode = x.DepartmentCode,
                            DivisionCode = x.DivisionCode,
                            Email = x.Email,
                            EmployeeCode = x.EmployeeCode,
                            FullName = x.FullName,
                            Gender = p != null ? p.Gender : Gender.Male,
                            GroupCode = x.GroupCode,
                            HospitalCode = x.HospitalCode,
                            IsActive = x.IsActive,
                            LiveNumber = x.LiveNumber,
                            LiveTime = x.LiveTime,
                            Name = x.Name,
                            PaymentStatusCode = x.PaymentStatusCode,
                            PhoneNumber = p != null ? p.PhoneNumber : null,
                            PositionCode = x.PositionCode,
                            QthtBlvp = x.QthtBlvp,
                            StatusCode = x.StatusCode,
                            UpdateDate = x.UpdateDate,
                            UseBiometric = p != null && p.UseBiometric,
                            UserBlvp = x.UserBlvp,
                            IsAdmin = p != null && p.IsAdmin,
                            QrCode = p != null ? p.QrCode : null,
                            Tel = p != null ? p.Tel : null,
                            QrAddress = p != null ? p.QrAddress : null,
                            Title = p != null ? p.Title : null,
                            IsAnonymous = p != null && p.IsAnonymous,
                            EmailPvi = x.EmailPvi
                        };

            return await query.FirstOrDefaultAsync();
        }

        public async Task UpdateRangeUserInfoAsync(IEnumerable<UserInfo> userInfos)
        {
            DemoDbContext.UpdateRange(userInfos);

            await DemoDbContext.SaveChangesAsync();
        }

        public async Task UpdateUserInfoAsync(UserInfo userInfo)
        {
            DemoDbContext.UserInfos.Update(userInfo);

            await DemoDbContext.SaveChangesAsync();
        }

        public async Task<List<UserInfoVoteDto>> GetAllUserInfoByCodesAsync(IEnumerable<string> codes = null)
        {
            return await (from x in DbSet.WhereIf(codes != null, x => codes.Contains(x.UserCode))
                          join p in DemoDbContext.UserInfos on x.UserCode equals p.UserCode into gr
                          from p in gr.DefaultIfEmpty()
                          select new UserInfoVoteDto
                          {
                              FullName = x.FullName,
                              Avatar = p != null ? p.Avatar : null,
                              UserCode = x.UserCode
                          }).ToListAsync();
        }
    }
}
