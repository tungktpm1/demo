﻿using Demo.Dtos.Users;
using Demo.Entities;
using Deno.Dtos.Users;

namespace Demo.Repositories.Users
{
    public interface IUserRepository : IEfCoreRepository<User>
    {
        Task<UserDto> GetUserByUserCodeAsync(string userCode);
        Task<UserDto> GetUserByEmailAsync(string email);
        Task<List<User>> GetAllUsersSortByNameAsync();
        Task<UserInfo> GetCurrentUserInfoAsync(string userCode);
        Task UpdateUserInfoAsync(UserInfo userInfo);
        Task AddUserInfoAsync(UserInfo userInfo);
        Task<List<UserInfo>> GetAllUserInfoAsync();
        Task UpdateRangeUserInfoAsync(IEnumerable<UserInfo> userInfos);
        Task<List<UserQrCodeDto>> GetAllUserHasQrCodeAsync();
        Task<List<UserInfoVoteDto>> GetAllUserInfoByCodesAsync(IEnumerable<string> codes = null);
    }
}
