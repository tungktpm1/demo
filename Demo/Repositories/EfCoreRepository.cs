﻿using Demo.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Demo.Repositories
{
    public class EfCoreRepository<TEntity> : IEfCoreRepository<TEntity> where TEntity : class
    {
        protected DemoDbContext DemoDbContext { get; }

        protected DbSet<TEntity> DbSet { get; }

        public EfCoreRepository(DemoDbContext demoDbContext)
        {
            DemoDbContext = demoDbContext;

            DbSet = DemoDbContext.Set<TEntity>();
        }

        public async Task<TEntity> AddAsync(TEntity entity, bool autoSave = false, CancellationToken cancellationToken = default)
        {
            var res = await DemoDbContext.Set<TEntity>().AddAsync(entity, cancellationToken);

            if (autoSave)
                await DemoDbContext.SaveChangesAsync(cancellationToken);

            return res.Entity;
        }

        public async Task DeleteAsync(TEntity entity, bool autoSave = false, CancellationToken cancellationToken = default)
        {
            DemoDbContext.Set<TEntity>().Remove(entity);

            if (autoSave)
                await DemoDbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task<List<TEntity>> GetAllAsync(bool isNoTracking = false, CancellationToken cancellationToken = default)
        {
            var query = GetDbSet();

            if (isNoTracking)
                query = query.AsNoTracking();

            return await query.ToListAsync(cancellationToken);
        }

        public async Task UpdateAsync(TEntity entity, bool autoSave = false, CancellationToken cancellationToken = default)
        {
            DemoDbContext.Set<TEntity>().Update(entity);

            if (autoSave)
                await DemoDbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task<bool> AnyAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken = default)
        {
            return await DemoDbContext.Set<TEntity>().AnyAsync(predicate, cancellationToken);
        }

        public async Task AddRangeAsync(IEnumerable<TEntity> entities, bool autoSave = false, CancellationToken cancellationToken = default)
        {
            await DemoDbContext.Set<TEntity>().AddRangeAsync(entities, cancellationToken);

            if (autoSave)
                await DemoDbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task UpdateRangeAsync(IEnumerable<TEntity> entities, bool autoSave = false, CancellationToken cancellationToken = default)
        {
            DemoDbContext.Set<TEntity>().UpdateRange(entities);

            if (autoSave)
                await DemoDbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task DeleteAsync(Expression<Func<TEntity, bool>> predicate, bool autoSave = false, CancellationToken cancellationToken = default)
        {
            var entities = await DemoDbContext.Set<TEntity>().Where(predicate).ToListAsync(cancellationToken);

            DemoDbContext.Set<TEntity>().RemoveRange(entities);

            if (autoSave)
                await DemoDbContext.SaveChangesAsync(cancellationToken);
        }

        public IQueryable<TEntity> GetDbSet() => DbSet;

        public async Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken = default)
        {
            return await DemoDbContext.Set<TEntity>().AsNoTracking().CountAsync(predicate, cancellationToken);
        }

        public async Task DeleteAsync(IEnumerable<TEntity> entities, bool autoSave = false, CancellationToken cancellationToken = default)
        {
            DemoDbContext.Set<TEntity>().RemoveRange(entities);
            
            if (autoSave)
                await DemoDbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken = default)
        {
            return await DbSet.FirstOrDefaultAsync(predicate, cancellationToken);
        }

        public IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.Where(predicate);
        }

        public IQueryable<TEntity> WhereIf(bool condition, Expression<Func<TEntity, bool>> predicate)
        {
            return condition ? DbSet.Where(predicate) : DbSet;
        }

        public async Task SaveChangeAsync()
        {
            await DemoDbContext.SaveChangesAsync();
        }
    }
}
