﻿using Demo.Entities;
using Demo.EntityFrameworkCore;

namespace Demo.Repositories.Notifications
{
    public class NotificationRepository : EfCoreRepository<Notification>, INotificationRepository
    {
        public NotificationRepository(DemoDbContext pVIReSmartDbContext)
            : base(pVIReSmartDbContext)
        {
        }
    }
}
