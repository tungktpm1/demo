﻿using Demo.Entities;

namespace Demo.Repositories.Notifications
{
    public interface INotificationRepository : IEfCoreRepository<Notification>
    {
    }
}
