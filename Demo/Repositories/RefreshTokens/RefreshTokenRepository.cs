﻿using Demo.Entities;
using Demo.EntityFrameworkCore;

namespace Demo.Repositories.RefreshTokens
{
    public class RefreshTokenRepository : EfCoreRepository<RefreshToken>, IRefreshTokenRepository
    {
        public RefreshTokenRepository(DemoDbContext DemoDbContext)
            : base(DemoDbContext)
        {
        }
    }
}
