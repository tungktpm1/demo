﻿using Demo.Entities;

namespace Demo.Repositories.RefreshTokens
{
    public interface IRefreshTokenRepository : IEfCoreRepository<RefreshToken>
    {
    }
}
