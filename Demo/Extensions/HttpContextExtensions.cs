﻿using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using Demo.Constants;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;

namespace Demo.Extensions
{
    public static class HttpContextExtensions
    {
        public static IPAddress GetRemoteIPAddress(this HttpContext context, bool allowForwarded = true)
        {
            try
            {
                if (allowForwarded)
                {
                    string header = context.Request?.Headers["CF-Connecting-IP"].FirstOrDefault() ?? context.Request?.Headers["X-Forwarded-For"].FirstOrDefault();
                    if (IPAddress.TryParse(header, out IPAddress ip))
                    {
                        return ip;
                    }
                }
                return context.Connection.RemoteIpAddress;
            }
            catch (Exception)
            {
            }
            return null;
        }

        public static IPAddress GetRemoteIPAddress(this IHttpContextAccessor httpContextAccessor, bool allowForwarded = true)
        {
            try
            {
                if (allowForwarded)
                {
                    string header = (httpContextAccessor.HttpContext?.Request?.Headers["CF-Connecting-IP"].FirstOrDefault() ?? httpContextAccessor.HttpContext?.Request?.Headers["X-Forwarded-For"].FirstOrDefault());
                    if (IPAddress.TryParse(header, out IPAddress ip))
                    {
                        return ip;
                    }
                }
                return httpContextAccessor.HttpContext?.Connection?.RemoteIpAddress;
            }
            catch (Exception)
            {
            }
            return null;
        }

        public static string GetUserCodeOfUserLogin(this IHttpContextAccessor httpContextAccessor)
        {
            bool isValidToken = httpContextAccessor.HttpContext.Request.Headers.TryGetValue(HeaderNames.Authorization, out var tokenString);

            if (!isValidToken || !tokenString.ToString().HasValue()) return default;

            var jwtEncodedString = tokenString.ToString()[7..];

            var token = new JwtSecurityToken(jwtEncodedString);

            var claim = token.Claims.FirstOrDefault(x => x.Type == ClaimTypeConst.UserCode);

            string userCode = claim.Value;

            return userCode;
        }

        public static string GetEmailOfUserLogin(this HttpContext httpContext)
        {
            bool isValidToken = httpContext.Request.Headers.TryGetValue(HeaderNames.Authorization, out var tokenString);

            if (!isValidToken || !tokenString.ToString().HasValue()) return default;

            var jwtEncodedString = tokenString.ToString()[7..];

            var token = new JwtSecurityToken(jwtEncodedString);

            var claim = token.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Email);

            string email = claim?.Value;

            return email;
        }

        public static bool IsAnonymous(this HttpContext httpContext)
        {
            bool isValidToken = httpContext.Request.Headers.TryGetValue(HeaderNames.Authorization, out var tokenString);

            if (!isValidToken || !tokenString.ToString().HasValue()) return default;

            var jwtEncodedString = tokenString.ToString()[7..];

            var token = new JwtSecurityToken(jwtEncodedString);

            var claim = token.Claims.FirstOrDefault(x => x.Type == ClaimTypeConst.Anonymous);

            bool isValid = bool.TryParse(claim?.Value, out var isAnonymous);

            return isValid && isAnonymous;
        }
    }
}
