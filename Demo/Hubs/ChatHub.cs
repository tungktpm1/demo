﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System.Security.Claims;

namespace Application.Hubs
{
    [Authorize]
    public class ChatHub : Hub
    {
        public ChatHub()
        {
        }

        public override async Task OnConnectedAsync()
        {
            //Todo
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            var userIdClaim = Context.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Email)?.Value;

            if (userIdClaim == null)
                return;

            var userId = Guid.Parse(userIdClaim);

            // Todo
        }

        public string GetConnectionId() => Context.ConnectionId;
    }

    public static class HubExtenstion
    {
        public static async Task SendNotificationJoinGroupToClientAsync(this IHubContext<ChatHub> hubContext, params Guid[] userIds)
        {
            userIds ??= Array.Empty<Guid>();

            foreach (var item in userIds)
            {
                await hubContext.Clients.Group(item.ToString()).SendAsync("ReJoinGroup");
            }
        }
    }
}
