﻿using System.ComponentModel;

namespace Demo.Enums
{
    public enum ApplicationLeaveStatus : byte
    {
        /// <summary>
        /// Đang chờ duyệt
        /// </summary>
        [Description("Đang chờ duyệt")]
        Pending = 0,
        /// <summary>
        /// Đã duyệt
        /// </summary>
        [Description("Phê duyệt")]
        Approve = 1
    }
}
