﻿namespace Demo.Enums
{
    public enum DeviceType : byte
    {
        Android = 0,
        IOS = 1
    }
}
