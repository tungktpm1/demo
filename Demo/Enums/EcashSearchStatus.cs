﻿using System.ComponentModel;

namespace Demo.Enums
{
    public enum EcashSearchStatus : byte
    {
        /// <summary>
        /// Chờ duyệt
        /// </summary>
        [Description("CHO_DUYET")]
        Pending = 0,
        /// <summary>
        /// Đã duyệt
        /// </summary>
        [Description("DA_DUYET")]
        Approved = 1,
        /// <summary>
        /// Tất cả
        /// </summary>
        [Description("ALL")]
        All = 2
    }
}
