﻿namespace Demo.Enums
{
    public enum EcashDocumentType : byte
    {
        /// <summary>
        /// Payable
        /// </summary>
        TTDT = 0,
        /// <summary>
        /// Receivable
        /// </summary>
        TT01 = 1,
        /// <summary>
        /// Other
        /// </summary>
        TT02 = 2
    }
}
