﻿namespace Demo.Enums
{
    public enum RepeatType : byte
    {
        /// <summary>
        /// Không lặp lại
        /// </summary>
        NotRepeat = 0,
        /// <summary>
        /// Theo tháng
        /// </summary>
        Month = 1,
        /// <summary>
        /// Theo năm
        /// </summary>
        Year = 2
    }
}
