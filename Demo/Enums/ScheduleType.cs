﻿namespace Demo.Enums
{
    public enum ScheduleType : byte
    {
        /// <summary>
        /// Lịch họp
        /// </summary>
        Meeting = 0,
        /// <summary>
        /// Lịch sự kiện
        /// </summary>
        Event = 1
    }
}
