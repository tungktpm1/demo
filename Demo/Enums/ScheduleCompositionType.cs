﻿namespace Demo.Enums
{
    [Flags]
    public enum ScheduleCompositionType : byte
    {
        Main = 1,
        CompositionCC = 2,
        Both = 3
    }
}
