﻿namespace Demo.Enums
{
    public enum GroupType
    {
        /// <summary>
        /// Danh mục nhóm
        /// </summary>
        GroupCategory = 0,
        /// <summary>
        /// Danh mục phòng họp
        /// </summary>
        MeetingRoom = 1,
        /// <summary>
        /// Danh mục loại sự kiện
        /// </summary>
        EventType = 2
    }
}
