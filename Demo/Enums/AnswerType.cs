﻿namespace Demo.Enums
{
    public enum AnswerType : byte
    {
        Choice = 0,
        Text = 1
    }
}
