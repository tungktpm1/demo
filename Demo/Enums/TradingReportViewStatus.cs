﻿namespace Demo.Enums
{
    public enum TradingReportViewStatus : byte
    {
        /// <summary>
        /// Tháng hiện tại
        /// </summary>
        CurrentMonth = 0,
        /// <summary>
        /// Lũy kế tháng
        /// </summary>
        Month = 1,
        /// <summary>
        /// Lũy kế năm
        /// </summary>
        Year = 2
    }
}
