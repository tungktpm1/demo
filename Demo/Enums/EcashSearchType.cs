﻿namespace Demo.Enums
{
    public enum EcashSearchType : byte
    {
        ToTrinh = 0,
        DNTT = 1,
        DNTT_TBH = 2
    }
}
