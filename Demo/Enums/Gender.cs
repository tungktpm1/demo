﻿namespace Demo.Enums
{
    public enum Gender : byte
    {
        /// <summary>
        /// Nam
        /// </summary>
        Male = 0,
        /// <summary>
        /// Nữ
        /// </summary>
        Female = 1
    }
}
