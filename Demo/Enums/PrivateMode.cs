﻿namespace Demo.Enums
{
    public enum PrivateMode : byte
    {
        Public = 0,
        Private = 1
    }
}
