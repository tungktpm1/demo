﻿namespace Demo.Enums
{
    public enum NotificationType : byte
    {
        /// <summary>
        /// Lịch
        /// </summary>
        Schedule = 0,
        /// <summary>
        /// Nghỉ phép
        /// </summary>
        DayOff = 1,
        /// <summary>
        /// Poll
        /// </summary>
        Poll = 2,
        /// <summary>
        /// Survey
        /// </summary>
        Survey = 3
    }
}
