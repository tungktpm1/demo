﻿namespace Demo.Enums
{
    public enum VoteType : byte
    {
        Poll = 0,
        Survey = 1
    }
}
