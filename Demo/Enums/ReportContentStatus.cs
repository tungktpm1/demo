﻿namespace Demo.Enums
{
    public enum ReportContentStatus
    {
        /// <summary>
        /// Lấy hết
        /// </summary>
        All = -1,
        /// <summary>
        /// Chưa duyệt
        /// </summary>
        NotApproved = 0,
        /// <summary>
        /// Đã duyệt
        /// </summary>
        Approved = 1
    }
}
