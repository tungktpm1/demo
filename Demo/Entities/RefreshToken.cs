﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Demo.Constants.EntityConsts;

namespace Demo.Entities
{
    [Table("refresh_token")]
    public class RefreshToken
    {
        [Key]
        [Column("pr_key")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [StringLength(UserConst.UserCodeLength)]
        [Column("ma_user")]
        public string UserCode { get; set; }
        [Required]
        [Column("token")]
        public string Token { get; set; }
        [Column("thoi_gian_tao")]
        public DateTime CreatedDate { get; set; }
        [Column("thoi_gian_ket_thuc")]
        public DateTime ExpiryDate { get; set; }
        [Column("thu_hoi")]
        public bool IsRevoked { get; set; }
        [Column("su_dung")]
        public bool IsUsed { get; set; }
    }
}
