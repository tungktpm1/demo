﻿using Demo.Constants.EntityConsts;
using Demo.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Demo.Entities
{
    [Table("thong_bao")]
    public class Notification
    {
        [Key]
        [Column("pr_key")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [StringLength(UserConst.UserCodeLength)]
        [Column("ma_user")]
        public string UserCode { get; set; }
        [Required]
        [Column("tieu_de")]
        public string Title { get; set; }
        [Required]
        [Column("noi_dung")]
        public string Content { get; set; }
        [Column("thoi_gian_tao")]
        public DateTime CreationTime { get; set; }
        [Column("da_doc")]
        public bool? IsRead { get; set; }
        [Column("loai_thong_bao")]
        public NotificationType NotificationType { get; set; }
        /// <summary>
        /// Id bản ghi được tham chiếu đến, để FE xử lý khi click vào thông báo sẽ nhảy sang detail
        /// </summary>
        [Column("ref_id")]
        public int RefId { get; set; }
        [Required]
        [Column("tham_so")]
        public string[] Arguments { get; set; } = Array.Empty<string>();
        [Required]
        [Column("tham_so_chi_so")]
        public int[] ArgumentIndexes { get; set; } = Array.Empty<int>();
    }
}
