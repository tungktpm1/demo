﻿using Demo.Constants.EntityConsts;
using Demo.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Demo.Entities
{
    [Table("dm_user_info")]
    public class UserInfo
    {
        [Key]
        [Column("pr_key")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [StringLength(UserConst.UserCodeLength)]
        [Column("ma_user")]
        public string UserCode { get; set; } = string.Empty;
        [StringLength(UserConst.AvatarLength)]
        [Column("avatar")]
        public string Avatar { get; set; }
        [Column("ngay_sinh")]
        public DateTime DateOfBirth { get; set; }
        [Column("gioi_tinh")]
        public Gender Gender { get; set; }
        [StringLength(UserConst.PhoneNumberLength)]
        [Column("sdt")]
        public string PhoneNumber { get; set; }
        [Column("strac_hoc")]
        public bool UseBiometric { get; set; }
        [Column("so_lan_login_fail")]
        public int NumberLoginFail { get; set; }
        [Column("thoi_gian_khoa")]
        public DateTime LockTime { get; set; }
        [Column("is_admin")]
        public bool IsAdmin { get; set; }
        [Column("qr_code")]
        public string QrCode { get; set; }
        [Column("chuc_danh")]
        public string Title { get; set; }
        [Column("tel")]
        public string Tel { get; set; }
        [Column("an_danh")]
        public bool IsAnonymous { get; set; }
        [Column("qr_address")]
        public string QrAddress { get; set; }
    }
}
