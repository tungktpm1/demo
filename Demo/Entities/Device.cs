﻿using Demo.Constants.EntityConsts;
using Demo.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Demo.Entities
{
    [Table("thiet_bi")]
    public class Device
    {
        [Key]
        [Column("pr_key")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [StringLength(UserConst.UserCodeLength)]
        [Column("ma_user")]
        public string UserCode { get; set; }
        [Required]
        [Column("token_thiet_bi")]
        public string DeviceToken { get; set; }
        [Column("loai_thiet_bi")]
        public DeviceType DeviceType { get; set; }
        [Column("thoi_gian_het_han")]
        public DateTime ExpiryTime { get; set; }
        [Required]
        [Column("ngon_ngu")]
        public string Language { get; set; }
    }
}
