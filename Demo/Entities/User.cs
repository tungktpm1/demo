﻿using Demo.Constants.EntityConsts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Demo.Entities
{
    [Table("dm_user")]
    public class User
    {
        [Key]
        [Required]
        [StringLength(UserConst.UserCodeLength)]
        [Column("ma_user")]
        public string UserCode { get; set; } = string.Empty;
        [Required]
        [StringLength(UserConst.NameLength)]
        [Column("ten_user")]
        public string Name { get; set; } = string.Empty;
        [Required]
        [StringLength(UserConst.FullNameLength)]
        [Column("full_name")]
        public string FullName { get; set; } = string.Empty;
        [Required]
        [StringLength(UserConst.PasswordLength)]
        [Column("password")]
        public string Password { get; set; } = string.Empty;
        [Required]
        [StringLength(UserConst.GroupCodeLength)]
        [Column("ma_nhom")]
        public string GroupCode { get; set; } = string.Empty;
        [Required]
        [StringLength(UserConst.DepartmentCodeLength)]
        [Column("ma_donvi")]
        public string DepartmentCode { get; set; } = string.Empty;
        [Required]
        [StringLength(UserConst.DivisionCodeLength)]
        [Column("ma_phong")]
        public string DivisionCode { get; set; } = string.Empty;
        [StringLength(UserConst.HospitalCodeLength)]
        [Column("ma_benhvien")]
        public string HospitalCode { get; set; }
        [Column("phan_quyen")]
        public bool Decentralization { get; set; }
        [Column("nguoidung_BLVP")]
        public bool? UserBlvp { get; set; }
        [Column("qtht_BLVP")]
        public bool? QthtBlvp { get; set; }
        [Required]
        [Column("ma_tthai")]
        [StringLength(UserConst.StatusCodeLength)]
        public string StatusCode { get; set; } = string.Empty;
        [Required]
        [Column("ma_ctu_arr")]
        [StringLength(UserConst.MaCtuArrLength)]
        public string MaCtuArr { get; set; } = string.Empty;
        [Required]
        [Column("ma_pkt_arr")]
        [StringLength(UserConst.MaPktArrLength)]
        public string MaPktArr { get; set; } = string.Empty;
        [Required]
        [Column("ma_pttai_arr")]
        [StringLength(UserConst.MaPttaiArrLength)]
        public string MaPttaiArr { get; set; } = string.Empty;
        [Column("ngay_cnhat")]
        public DateTime? UpdateDate { get; set; }
        [StringLength(UserConst.PaymentStatusCodeLength)]
        [Column("ma_tthai_ttoan")]
        public string PaymentStatusCode { get; set; } = string.Empty;
        [Column("trang_thai")]
        public bool IsActive { get; set; }
        [Required]
        [Column("email")]
        [StringLength(UserConst.EmailLength)]
        public string Email { get; set; } = string.Empty;
        [Column("ma_chucvu")]
        [Required]
        [StringLength(UserConst.PositionCodeLength)]
        public string PositionCode { get; set; } = string.Empty;
        [Required]
        [StringLength(UserConst.EmployeeCodeLength)]
        [Column("ma_cbcnv")]
        public string EmployeeCode { get; set; } = string.Empty;
        [Required]
        [StringLength(UserConst.MaKHLength)]
        [Column("ma_kh")]
        public string MaKH { get; set; } = string.Empty;
        [Required]
        [StringLength(UserConst.PasswordSignLength)]
        [Column("password_sign")]
        public string PasswordSign { get; set; }
        [StringLength(UserConst.PasswordSha256Length)]
        [Column("password_sha256")]
        public string PasswordSha256 { get; set; }
        [Required]
        [StringLength(UserConst.OtpCodeLength)]
        [Column("OTP_code")]
        public string OtpCode { get; set; } = string.Empty;
        [Column("OTP_disable")]
        public bool IsDisableOtp { get; set; }
        [Column("live_time")]
        public DateTime? LiveTime { get; set; }
        [Column("live_num")]
        public int LiveNumber { get; set; }
        public string EmailPvi { get; set; }
    }
}
