﻿using System.ComponentModel.DataAnnotations;

namespace Demo.Dtos
{
    public class FileRequest
    {
        [Required]
        public IFormFile File { get; set; }
    }
}
