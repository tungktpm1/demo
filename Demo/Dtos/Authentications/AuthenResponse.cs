﻿namespace Demo.Dtos.Authentications
{
    public class AuthenResponse
    {
        public string AccessToken { get; set; }
        public int ExpriesIn { get; set; }
        public string TokenType { get; set; }
        public string RefreshToken { get; set; }
    }
}
