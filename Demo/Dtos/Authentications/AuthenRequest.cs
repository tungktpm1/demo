﻿using System.ComponentModel.DataAnnotations;

namespace Demo.Dtos.Authentications
{
    public class AuthenRequest
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
