﻿using System.ComponentModel.DataAnnotations;

namespace Demo.Dtos.Authentications
{
    public class TokenDto
    {
        public string AccessToken { get; set; }
        public string Sign { get; set; }
    }

    public class TokenRequest
    {
        [Required]
        public string AccessToken { get; set; }
        [Required]
        public string RefreshToken { get; set; }
    }
}
