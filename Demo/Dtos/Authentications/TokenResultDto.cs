﻿namespace Demo.Dtos.Authentications
{
    public class TokenResultDto : TokenBaseResultDto
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public DateTime RefreshTokenExpire { get; set; }
    }

    public class TokenBaseResultDto
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
