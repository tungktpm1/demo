﻿using System.ComponentModel.DataAnnotations;

namespace Demo.Dtos.Authentications
{
    public class RefreshTokenRequest
    {
        [Required]
        public string RefreshToken { get; set; }
    }
}
