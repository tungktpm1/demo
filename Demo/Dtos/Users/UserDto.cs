﻿using Demo.Enums;

namespace Demo.Dtos.Users
{
    public class UserDto
    {
        public string UserCode { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public string FullName { get; set; } = string.Empty;
        public string GroupCode { get; set; } = string.Empty;
        public string DepartmentCode { get; set; } = string.Empty;
        public string DivisionCode { get; set; } = string.Empty;
        public string HospitalCode { get; set; }
        public bool Decentralization { get; set; }
        public bool? UserBlvp { get; set; }
        public bool? QthtBlvp { get; set; }
        public string StatusCode { get; set; } = string.Empty;
        public string MaCtuArr { get; set; } = string.Empty;
        public string MaPktArr { get; set; } = string.Empty;
        public string MaPttaiArr { get; set; } = string.Empty;
        public DateTime? UpdateDate { get; set; }
        public string PaymentStatusCode { get; set; } = string.Empty;
        public bool IsActive { get; set; }
        public string Email { get; set; } = string.Empty;
        public string PositionCode { get; set; } = string.Empty;
        public string EmployeeCode { get; set; } = string.Empty;
        public string MaKH { get; set; } = string.Empty;
        public string OtpCode { get; set; } = string.Empty;
        public bool IsDisableOtp { get; set; }
        public DateTime? LiveTime { get; set; }
        public int LiveNumber { get; set; }
        public string Role { get; set; }
        public DateTime DateOfBirth { get; set; }
        public Gender Gender { get; set; }
        public string Avatar { get; set; }
        public string PhoneNumber { get; set; }
        public bool UseBiometric { get; set; }
        public bool IsAdmin { get; set; }
        public string QrCode { get; set; }
        public string Title { get; set; }
        public string Tel { get; set; }
        public bool IsAnonymous { get; set; }
        public string QrAddress { get; set; }
        public string EmailPvi { get; set; }
    }
}
