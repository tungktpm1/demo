﻿namespace Deno.Dtos.Users
{
    public class UserQrCodeDto
    {
        public string FullName { get; set; }
        public string QrCode { get; set; }
    }
}
