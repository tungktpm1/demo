﻿namespace Demo.Dtos.Users
{
    public class UserInfoDto
    {
        public string UserCode { get; set; }
        public string FullName { get; set; }
    }

    public class UserInfoVoteDto : UserInfoDto
    {
        public int VoteId { get; set; }

        public string Avatar { get; set; }
    }
}
