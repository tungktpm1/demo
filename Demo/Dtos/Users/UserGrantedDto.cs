﻿namespace Demo.Dtos.Users
{
    public class UserGrantedDto
    {
        public string UserCode { get; set; }
        public string DepartmentCode { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public bool IsGranted { get; set; }
    }
}
