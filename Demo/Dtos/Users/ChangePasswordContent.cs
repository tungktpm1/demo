﻿namespace Demo.Dtos.Users
{
    public class ChangePasswordContent
    {
        public string Password { get; set; }
        public string NewPassword { get; set; }
    }
}
