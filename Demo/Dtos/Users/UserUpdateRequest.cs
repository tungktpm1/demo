﻿using Demo.Constants.EntityConsts;
using Demo.Enums;
using System.ComponentModel.DataAnnotations;

namespace Deno.Dtos.Users
{
    public class UserUpdateRequest
    {
        [Required]
        [StringLength(UserConst.FullNameLength)]
        public string FullName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public Gender Gender { get; set; }
        [StringLength(UserConst.PhoneNumberLength)]
        public string PhoneNumber { get; set; }
        [Required]
        [StringLength(UserConst.EmailLength)]
        public string Email { get; set; }
        public bool UseBiometric { get; set; }
        public string Avatar { get; set; }
        public IFormFile Image { get; set; }
        public string Title { get; set; }
        public string Tel { get; set; }
    }
}
