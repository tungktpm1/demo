﻿using Demo.Enums;

namespace Demo.Dtos.Devices
{
    public class DeviceDto
    {
        public string UserCode { get; set; }
        public string DeviceToken { get; set; }
        public DeviceType DeviceType { get; set; }
        public DateTime ExpiryTime { get; set; }
        public int ScheduleId { get; set; }
    }
}
