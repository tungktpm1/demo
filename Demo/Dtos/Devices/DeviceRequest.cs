﻿using Demo.Enums;
using System.ComponentModel.DataAnnotations;

namespace Demo.Dtos.Devices
{
    public class DeviceRequest
    {
        [Required]
        public string DeviceToken { get; set; }
        public DeviceType DeviceType { get; set; }
        [Required]
        public string Language { get; set; }
    }
}
