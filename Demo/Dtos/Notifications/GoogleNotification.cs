﻿using Newtonsoft.Json;
using Demo.Enums;

namespace Demo.Dtos.Notifications
{
    public class GoogleNotification
    {
        public class DataPayload
        {
            [JsonProperty("title")]
            public string Title { get; set; }
            [JsonProperty("body")]
            public string Body { get; set; }
            [JsonProperty("notificationType")]
            public NotificationType NotificationType { get; set; }
            [JsonProperty("id")]
            public int Id { get; set; }
            [JsonProperty("badge")]
            public int Badge { get; set; }
        }

        [JsonProperty("priority")]
        public string Priority { get; set; } = "high";
        [JsonProperty("data")]
        public DataPayload Data { get; set; }
        [JsonProperty("notification")]
        public DataPayload Notification { get; set; }
    }
}
