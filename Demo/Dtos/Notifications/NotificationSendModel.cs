﻿using Demo.Enums;
using System.Text.Json.Serialization;

namespace Demo.Dtos.Notifications
{
    public class NotificationSendModel
    {
        public string DeviceId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public int Id { get; set; }
        [JsonIgnore]
        public string UserCode { get; set; }
        public NotificationType NotificationType { get; set; }
        public int Badge { get; set; }
    }
}
