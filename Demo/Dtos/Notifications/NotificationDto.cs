﻿using Demo.Enums;

namespace Demo.Dtos.Notifications
{
    public class NotificationDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime CreationTime { get; set; }
        public bool? IsRead { get; set; }
        public NotificationType NotificationType { get; set; }
        public int RefId { get; set; }
        public string[] Arguments { get; set; }
        public int[] ArgumentIndexes { get; set; }
    }
}
