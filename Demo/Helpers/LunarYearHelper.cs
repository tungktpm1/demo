﻿namespace Demo.Helpers
{
    public static class LunarYearHelper
    {
        enum Can { Giáp, Ất, Bính, Đinh, Mậu, Kỷ, Canh, Tân, Nhâm, Quý }
        enum Chi { Tý, Sửu, Dần, Mão, Thìn, Tỵ, Ngọ, Mùi, Thân, Dậu, Tuất, Hợi }

        public static string CanChiNam(int year)
        {
            return (Can)((year + 6) % 10) + " " + (Chi)((year + 8) % 12) + " (" + year + ")";
        }

        public static string CanChiThang(int month, int year)
        {
            return (Can)((year * 12 + month + 3) % 10) + " " + (Chi)((month + 1) % 12);
        }

        public static int Int(double d)
        {
            return (int)Math.Floor(d);
        }

        public static int Mod(int x, int y)
        {
            int z = x - (int)(y * Math.Floor((double)x / y));

            return z == 0 ? y : z;
        }

        /// <summary>
        /// Đổi ngày dương lịch ra số ngày Julius
        /// </summary>
        /// <param name="day"></param>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public static double SolarToJD(int day, int month, int year)
        {
            double JD;
            if (year > 1582 || (year == 1582 && month > 10) || (year == 1582 && month == 10 && day > 14))
            {
                JD = 367 * year - Int(7 * (year + Int((month + 9) / 12)) / 4) - Int(3 * (Int((year + (month - 9) / 7) / 100) + 1) / 4) + Int(275 * month / 9) + day + 1721028.5;
            }
            else
            {
                JD = 367 * year - Int(7 * (year + 5001 + Int((month - 9) / 7)) / 4) + Int(275 * month / 9) + day + 1729776.5;
            }
            return JD;
        }

        /// <summary>
        /// Đổi số ngày Julius ra ngày dương lịch
        /// </summary>
        /// <param name="jd"></param>
        /// <returns></returns>
        public static (int Day, int Month, int Year) SolarFromJD(double jd)
        {
            var z = Int(jd + 0.5);

            var f = jd + 0.5 - z;

            var alpha = Int((z - 1867216.25) / 36524.25);

            var a = z < 2299161 ? z : (z + 1 + alpha - Int(alpha / 4));

            var b = a + 1524;

            var c = Int((b - 122.1) / 365.25);

            var d = Int(365.25 * c);

            var e = Int((b - d) / 30.6001);

            var day = Int(b - d - Int(30.6001 * e) + f);

            var month = e < 14 ? (e - 1) : (e - 13);

            var year = month < 3 ? (c - 4715) : (c - 4716);

            return (day, month, year);
        }

        /// <summary>
        /// Chuyển đổi số ngày Julius / ngày dương lịch theo giờ địa phương LOCAL_TIMEZONE, Việt Nam: LOCAL_TIMEZONE = 7.0
        /// </summary>
        /// <param name="jd"></param>
        /// <returns></returns>
        public static (int Day, int Month, int Year) LocalFromJD(double jd)
        {
            return SolarFromJD(jd + (7.0 / 24.0));
        }

        public static double LocalToJD(int day, int month, int year)
        {
            return SolarToJD(day, month, year) - (7.0 / 24.0);
        }

        /// <summary>
        /// Tính thời điểm Sóc
        /// </summary>
        /// <param name="k"></param>
        /// <returns></returns>
        public static double NewMoon(int k)
        {
            double t = k / 1236.85;

            double t2 = t * t;

            double t3 = t2 * t;

            double dr = Math.PI / 180;

            double jd1 = 2415020.75933 + 29.53058868 * k + 0.0001178 * t2 - 0.000000155 * t3 + 0.00033 * Math.Sin((166.56 + 132.87 * t - 0.009173 * t2) * dr);

            double m = 359.2242 + 29.10535608 * k - 0.0000333 * t2 - 0.00000347 * t3;

            double mpr = 306.0253 + 385.81691806 * k + 0.0107306 * t2 + 0.00001236 * t3;

            double f = 21.2964 + 390.67050646 * k - 0.0016528 * t2 - 0.00000239 * t3;

            double c1 = (0.1734 - 0.000393 * t) * Math.Sin(m * dr) + 0.0021 * Math.Sin(2 * dr * m);

            c1 = c1 - 0.4068 * Math.Sin(mpr * dr) + 0.0161 * Math.Sin(dr * 2 * mpr);

            c1 -= 0.0004 * Math.Sin(dr * 3 * mpr);

            c1 = c1 + 0.0104 * Math.Sin(dr * 2 * f) - 0.0051 * Math.Sin(dr * (m + mpr));

            c1 = c1 - 0.0074 * Math.Sin(dr * (m - mpr)) + 0.0004 * Math.Sin(dr * (2 * f + m));

            c1 = c1 - 0.0004 * Math.Sin(dr * (2 * f - m)) - 0.0006 * Math.Sin(dr * (2 * f + mpr));

            c1 = c1 + 0.0010 * Math.Sin(dr * (2 * f - mpr)) + 0.0005 * Math.Sin(dr * (2 * mpr + m));

            double deltaT = t < -11 ? (0.001 + 0.000839 * t + 0.0002261 * t2 - 0.00000845 * t3 - 0.000000081 * t * t3) : (-0.000278 + 0.000265 * t + 0.000262 * t2);

            return jd1 + c1 - deltaT;
        }

        /// <summary>
        /// Tính vị trí của mặt trời
        /// </summary>
        /// <param name="jdn"></param>
        /// <returns></returns>
        public static double SunLongitude(double jdn)
        {
            double t = (jdn - 2451545.0) / 36525;

            double t2 = t * t;

            double dr = Math.PI / 180;

            double m = 357.52910 + 35999.05030 * t - 0.0001559 * t2 - 0.00000048 * t * t2;

            double l0 = 280.46645 + 36000.76983 * t + 0.0003032 * t2;

            double dl = (1.914600 - 0.004817 * t - 0.000014 * t2) * Math.Sin(dr * m) + (0.019993 - 0.000101 * t) * Math.Sin(dr * 2 * m) + 0.000290 * Math.Sin(dr * 3 * m);

            double l = (l0 + dl) * dr;

            l -= Math.PI * 2 * Int(l / (Math.PI * 2));

            return l;
        }

        /// <summary>
        /// Tính tháng âm lịch chứa ngày Đông chí
        /// </summary>
        /// <param name="Y"></param>
        /// <returns></returns>
        public static (int Day, int Month, int Year) LunarMonth11(int Y)
        {
            double off = LocalToJD(31, 12, Y) - 2415021.076998695;

            int k = Int(off / 29.530588853);

            double jd = NewMoon(k);

            var ret = LocalFromJD(jd);

            double sunLong = SunLongitude(LocalToJD(ret.Day, ret.Month, ret.Year));

            if (sunLong > 3 * Math.PI / 2)
            {
                jd = NewMoon(k - 1);
            }

            return LocalFromJD(jd);
        }

        /// <summary>
        /// Tính năm âm lịch
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        public static int[][] LunarYear(int year)
        {
            var month11A = LunarMonth11(year - 1);

            double jdMonth11A = LocalToJD(month11A.Day, month11A.Month, month11A.Year);

            int k = (int)Math.Floor(0.5 + (jdMonth11A - 2415021.076998695) / 29.530588853);

            var month11B = LunarMonth11(year);

            double off = LocalToJD(month11B.Day, month11B.Month, month11B.Year) - jdMonth11A;

            bool leap = off > 365.0;

            var ret = !leap ? new int[13][] : new int[14][];

            ret[0] = new int[] { month11A.Day, month11A.Month, month11A.Year, 0, 0 };

            ret[^1] = new int[] { month11B.Day, month11B.Month, month11B.Year, 0, 0 };

            for (int i = 1; i < ret.Length - 1; i++)
            {
                double nm = NewMoon(k + i);

                var a = LocalFromJD(nm);

                ret[i] = new int[] { a.Day, a.Month, a.Year, 0, 0 };
            }

            for (int i = 0; i < ret.Length; i++)
            {
                ret[i][3] = Mod(i + 11, 12);
            }

            if (leap)
            {
                InitLeapYear(ret);
            }

            return ret;
        }

        /// <summary>
        /// Tính tháng nhuận
        /// </summary>
        /// <param name="ret"></param>
        public static void InitLeapYear(int[][] ret)
        {
            double[] sunLongitudes = new double[ret.Length];

            for (int i = 0; i < ret.Length; i++)
            {
                var a = ret[i];

                double jdAtMonthBegin = LocalToJD(a[0], a[1], a[2]);

                sunLongitudes[i] = SunLongitude(jdAtMonthBegin);
            }

            bool found = false;

            for (int i = 0; i < ret.Length; i++)
            {
                if (found)
                {
                    ret[i][3] = Mod(i + 10, 12);

                    continue;
                }

                double sl1 = sunLongitudes[i];

                double sl2 = sunLongitudes[i + 1];

                bool hasMajorTerm = Math.Floor(sl1 / Math.PI * 6) != Math.Floor(sl2 / Math.PI * 6);

                if (!hasMajorTerm)
                {
                    found = true;

                    ret[i][4] = 1;

                    ret[i][3] = Mod(i + 10, 12);
                }
            }
        }

        /// <summary>
        /// Lịch dương sang lịch âm
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static (int Day, int Month, int Year, bool IsLeapYear) Solar2Lunar(DateTime time)
        {
            var day = time.Day;

            var month = time.Month;

            var year = time.Year;

            int yy = year;

            int[][] ly = LunarYear(year);

            int[] month11 = ly[ly.Length - 1];

            double jdToday = LocalToJD(day, month, year);

            double jdMonth11 = LocalToJD(month11[0], month11[1], month11[2]);

            if (jdToday >= jdMonth11)
            {
                ly = LunarYear(year + 1);

                yy = year + 1;
            }

            int i = ly.Length - 1;

            while (jdToday < LocalToJD(ly[i][0], ly[i][1], ly[i][2]))
            {
                --i;
            }

            int dd = (int)(jdToday - LocalToJD(ly[i][0], ly[i][1], ly[i][2])) + 1;

            int mm = ly[i][3];

            if (mm >= 11)
                --yy;

            return (dd, mm, yy, ly[i][4] == 1);
        }
    }
}
