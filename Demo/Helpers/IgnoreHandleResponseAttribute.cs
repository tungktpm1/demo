﻿namespace Demo.Helpers
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class IgnoreHandleResponseAttribute : Attribute
    {
    }
}
