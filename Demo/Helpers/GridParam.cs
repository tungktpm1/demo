﻿using System.ComponentModel.DataAnnotations;

namespace Demo.Helpers
{
    public class GridParam
    {
        [Range(1, int.MaxValue)]
        public int MaxResultCount { get; set; }
        [Range(0, int.MaxValue)]
        public int SkipCount { get; set; }
    }
}
