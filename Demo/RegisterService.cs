﻿using AutoMapper;
using CorePush.Apple;
using CorePush.Google;
using Microsoft.Extensions.Localization;
using NetCore.AutoRegisterDi;
using Demo.Localizers;
using Demo.Middlewares;
using Demo.Repositories;
using Demo.Services;
using Demo.Services.Backgrounds;
using System.Reflection;

namespace Demo
{
    public static class RegisterService
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddHttpClient<FcmSender>();

            services.AddHttpClient<ApnSender>();

            services.AddSingleton<IStringLocalizerFactory, JsonStringLocalizerFactory>();

            services.AddSingleton<LocalizationMiddleware>();

            services.AddScoped(typeof(IEfCoreRepository<>), typeof(EfCoreRepository<>));

            var assembliesToScans = new[]
            {
                Assembly.GetAssembly(typeof(ApplicationServiceBase)),
            };

            services.RegisterAssemblyPublicNonGenericClasses(assembliesToScans)
            .Where(x => x.Name.EndsWith("Repository") || x.Name.EndsWith("AppService"))
              .AsPublicImplementedInterfaces(ServiceLifetime.Scoped);

            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            var mapper = mapperConfig.CreateMapper();

            services.AddSingleton(mapper);

            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();

            services.AddHostedService<DemoBackgroundService>();
        }
    }
}
