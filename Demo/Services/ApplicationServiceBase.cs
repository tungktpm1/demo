﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Demo.Dtos.Users;
using Demo.ExceptionHandling;
using Demo.Extensions;
using Demo.Repositories.Users;
using System.Globalization;

namespace Demo.Services
{
    public abstract class ApplicationServiceBase
    {
        protected IConfiguration Configuration { get; }
        protected IMapper Mapper { get; }
        protected IStringLocalizer<ApplicationServiceBase> L { get; }
        protected IUserRepository UserRepository { get; }
        protected IHttpContextAccessor HttpContext { get; }

        public ApplicationServiceBase(
            IConfiguration configuration,
            IMapper mapper,
            IStringLocalizer<ApplicationServiceBase> l,
            IUserRepository userRepository,
            IHttpContextAccessor httpContext
        )
        {
            Configuration = configuration;

            Mapper = mapper;

            L = l;

            UserRepository = userRepository;

            HttpContext = httpContext;
        }

        protected string Email => HttpContext.HttpContext.GetEmailOfUserLogin();

        protected async Task<string> GetUserCodeOfCurrentUserAsync()
        {
            return (await GetCurrentUserAsync())!.UserCode;
        }

        protected bool IsAnonymousUser => GetCurrentUserAsync().Result.IsAnonymous;

        protected virtual async Task<UserDto> GetCurrentUserAsync()
        {
            string email = Email;

            var user = await UserRepository.GetUserByEmailAsync(email);

            if (user == null) throw new UserFriendlyException(L["UserNotFound"]);

            return user;
        }

        protected bool DoesCultureExist(string cultureName)
        {
            return CultureInfo.GetCultures(CultureTypes.AllCultures)
                .Any(culture => string.Equals(culture.Name, cultureName,
                        StringComparison.CurrentCultureIgnoreCase));
        }

        protected void SetCurrentLanguage(string language)
        {
            if (!$"{language}".HasValue())
                language = "vi";

            if (DoesCultureExist(language))
            {
                var culture = new CultureInfo(language);

                Thread.CurrentThread.CurrentCulture = culture;

                Thread.CurrentThread.CurrentUICulture = culture;
            }
        }
    }
}
