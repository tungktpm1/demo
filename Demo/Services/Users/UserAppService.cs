﻿using AutoMapper;
using Demo.Constants.DemoApiStatusConsts;
using Demo.Dtos;
using Demo.Dtos.Users;
using Demo.Entities;
using Demo.ExceptionHandling;
using Demo.Extensions;
using Demo.Helpers;
using Demo.Repositories.Users;
using Deno.Dtos.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System.IO.Compression;

namespace Demo.Services.Users
{
    public class UserAppService : ApplicationServiceBase, IUserAppService
    {
        private AppSettings AppSettings { get; }
        private static string[] SupportedTypes { get; } = new[] { "jpg", "jpeg", "png", "gif" };

        public UserAppService(
            IConfiguration configuration,
            IMapper mapper,
            IStringLocalizer<ApplicationServiceBase> l,
            IUserRepository userRepository,
            IOptions<AppSettings> appSettings,
            IHttpContextAccessor httpContext
        ) : base(configuration, mapper, l, userRepository, httpContext)
        {
            AppSettings = appSettings.Value;
        }

        public async Task<List<UserDto>> GetAllUsersAsync()
        {
            var users = await UserRepository.GetDbSet()
                                .AsNoTracking()
                                .OrderBy(x => x.FullName)
                                .ToListAsync();

            return Mapper.Map<List<UserDto>>(users);
        }

        public async Task<UserDto> GetCurrentUserLoginAsync()
        {
            var user = await base.GetCurrentUserAsync();

            user.Role = user.IsAdmin ? "admin" : "user";

            if (user.Avatar.HasValue())
                user.Avatar = $"{Configuration["AppUrl"]}{user.Avatar}";

            return user;
        }

        public async Task UpdateAsync(UserUpdateRequest input)
        {
            var userCode = await GetUserCodeOfCurrentUserAsync();

            var user = await UserRepository.GetDbSet().FirstOrDefaultAsync(x => x.UserCode == userCode);

            if (user == null)
                throw new UserFriendlyException(L["UserNotFound"]);

            var userInfo = await UserRepository.GetCurrentUserInfoAsync(user.UserCode);

            bool isCreated = userInfo == null;

            if (isCreated)
                userInfo = new UserInfo
                {
                    UserCode = user.UserCode
                };

            userInfo.UseBiometric = input.UseBiometric;

            userInfo.DateOfBirth = input.DateOfBirth;

            userInfo.PhoneNumber = input.PhoneNumber;

            userInfo.Gender = input.Gender;

            userInfo.Title = input.Title;

            userInfo.Tel = input.Tel;

            if (input.Image != null)
            {
                string fileLocation = UploadFiles.CreateFolderIfNotExists("wwwroot", "Images");

                var fileExt = Path.GetExtension(input.Image.FileName)[1..].ToLower();

                if (!SupportedTypes.Contains(fileExt))
                    throw new UserFriendlyException(L["InvalidFile"]);

                string fileName = await UploadFiles.UploadAsync(fileLocation, input.Image);

                userInfo.Avatar = $"/Images/{fileName}";
            }
            else
                userInfo.Avatar = input.Avatar?.Replace(Configuration["AppUrl"], "");

            if (isCreated)
                await UserRepository.AddUserInfoAsync(userInfo);
            else
                await UserRepository.UpdateUserInfoAsync(userInfo);
        }

        public async Task ChangePasswordAsync(ChangePasswordContent input)
        {
            var userCode = await GetUserCodeOfCurrentUserAsync();

            //Todo
        }

        public async Task<UserDto> GetUserByCodeAsync(string code)
        {
            var user = await UserRepository.GetUserByUserCodeAsync(code);

            if (user == null) throw new UserFriendlyException("Không tồn tại người dùng");

            user.Role = user.IsAdmin ? "admin" : "user";

            if (user.Avatar.HasValue())
                user.Avatar = $"{Configuration["AppUrl"]}{user.Avatar}";

            return user;
        }

        public async Task<byte[]> DownloadQrCodeAsync()
        {
            var users = await UserRepository.GetAllUserHasQrCodeAsync();

            using var compressedFileStream = new MemoryStream();

            int i = 1;

            using (var zipArchive = new ZipArchive(compressedFileStream, ZipArchiveMode.Create, false))
            {
                foreach (var item in users)
                {
                    var zipEntry = zipArchive.CreateEntry($"{item.FullName}_{i}.jpg");

                    string base64 = item.QrCode.Replace("data:image/png;base64,", "");

                    using (var originalFileStream = new MemoryStream(Convert.FromBase64String(base64)))
                    {
                        using (var zipEntryStream = zipEntry.Open())
                        {
                            originalFileStream.CopyTo(zipEntryStream);
                        }
                    }

                    ++i;
                }
            }

            var byteZip = compressedFileStream.ToArray();

            return byteZip;
        }
    }
}
