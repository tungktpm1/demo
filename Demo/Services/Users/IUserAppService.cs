﻿using Demo.Dtos.Users;
using Deno.Dtos.Users;

namespace Demo.Services.Users
{
    public interface IUserAppService
    {
        Task<UserDto> GetCurrentUserLoginAsync();
        Task<UserDto> GetUserByCodeAsync(string code);

        Task<List<UserDto>> GetAllUsersAsync();

        Task UpdateAsync(UserUpdateRequest input);
        Task ChangePasswordAsync(ChangePasswordContent input);
        Task<byte[]> DownloadQrCodeAsync();
    }
}
