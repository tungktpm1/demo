﻿using Demo.Dtos;
using Demo.Extensions;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using Serilog;
using System.Globalization;

namespace Demo.Services.Backgrounds
{
    public class DemoBackgroundService : IHostedService, IDisposable
    {
        private Timer Timer { get; set; }

        private IServiceProvider ServiceProvider { get; }

        private AppSettings AppSettings { get; }

        private IStringLocalizer<DemoBackgroundService> L { get; }

        public DemoBackgroundService(IServiceProvider serviceProvider, IOptions<AppSettings> options, IStringLocalizer<DemoBackgroundService> l)
        {
            ServiceProvider = serviceProvider;

            AppSettings = options.Value;

            L = l;
        }

        public void Dispose()
        {
            Timer?.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Log.Information("Background Service running.");

            Timer = new Timer(async state =>
            {
                try
                {
                    
                }
                catch (Exception ex)
                {
                    Log.Error($"\n\nSend notification fail {DateTime.Now:dd/MM/yyyy HH:mm:ss}\n\n{ex.Message} \n\n{ex.StackTrace}");
                }
            },
            null,
            TimeSpan.Zero,
            TimeSpan.FromMinutes(1));

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Log.Information("Background Service is stopping.");

            Timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        private void SetCurrentLanguage(string language)
        {
            if (!$"{language}".HasValue())
                language = "vi";

            if (DoesCultureExist(language))
            {
                var culture = new CultureInfo(language);

                Thread.CurrentThread.CurrentCulture = culture;

                Thread.CurrentThread.CurrentUICulture = culture;
            }
        }

        private bool DoesCultureExist(string cultureName)
        {
            return CultureInfo.GetCultures(CultureTypes.AllCultures)
                .Any(culture => string.Equals(culture.Name, cultureName,
                        StringComparison.CurrentCultureIgnoreCase));
        }
    }
}
