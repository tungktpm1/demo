﻿using Demo.Dtos.Devices;

namespace Demo.Services.Devices
{
    public interface IDeviceAppService
    {
        Task AddAsync(DeviceRequest input);

        Task DeleteAsync(DeviceRequest input);
    }
}
