﻿using AutoMapper;
using Demo.Dtos.Devices;
using Demo.Entities;
using Demo.Repositories.Devices;
using Demo.Repositories.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

namespace Demo.Services.Devices
{
    public class DeviceAppService : ApplicationServiceBase, IDeviceAppService
    {
        private IDeviceRepository DeviceRepository { get; }

        public DeviceAppService(IConfiguration configuration,
            IMapper mapper,
            IStringLocalizer<ApplicationServiceBase> l,
            IUserRepository userRepository,
            IHttpContextAccessor httpContext,
            IDeviceRepository deviceRepository
        ) : base(configuration, mapper, l, userRepository, httpContext)
        {
            DeviceRepository = deviceRepository;
        }

        public async Task AddAsync(DeviceRequest input)
        {
            var userCode = await GetUserCodeOfCurrentUserAsync();

            var device = await DeviceRepository.GetDbSet().FirstOrDefaultAsync(x => x.DeviceToken == input.DeviceToken);

            bool isExists = device != null;

            if (!isExists)
            {
                device = new Device
                {
                    DeviceToken = input.DeviceToken,
                    DeviceType = input.DeviceType,
                    UserCode = userCode,
                    ExpiryTime = DateTime.Now.AddMonths(1),
                    Language = input.Language
                };

                await DeviceRepository.AddAsync(device, true);
            }
            else
            {
                device.ExpiryTime = DateTime.Now.AddMonths(1);

                device.UserCode = userCode;

                device.Language = input.Language;

                await DeviceRepository.UpdateAsync(device, true);
            }
        }

        public async Task DeleteAsync(DeviceRequest input)
        {
            var userCode = await GetUserCodeOfCurrentUserAsync();

            await DeviceRepository.DeleteAsync(x => x.UserCode == userCode && x.DeviceToken == input.DeviceToken, true);
        }
    }
}
