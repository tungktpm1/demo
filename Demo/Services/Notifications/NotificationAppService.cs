﻿using AutoMapper;
using CorePush.Google;
using Demo.Constants;
using Demo.Dtos;
using Demo.Dtos.Notifications;
using Demo.Enums;
using Demo.ExceptionHandling;
using Demo.Helpers;
using Demo.Repositories.Notifications;
using Demo.Repositories.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using Serilog;
using System.Net.Http.Headers;
using static Demo.Dtos.Notifications.GoogleNotification;

namespace Demo.Services.Notifications
{
    public class NotificationAppService : ApplicationServiceBase, INotificationAppService
    {
        private FcmNotificationSetting FcmNotificationSetting { get; }

        private INotificationRepository NotificationRepository { get; }

        public NotificationAppService(
            IConfiguration configuration,
            IMapper mapper,
            IStringLocalizer<ApplicationServiceBase> l,
            IUserRepository userRepository,
            IHttpContextAccessor httpContext,
            IOptions<AppSettings> appSettings,
            INotificationRepository notificationRepository
        ) : base(configuration, mapper, l, userRepository, httpContext)
        {
            FcmNotificationSetting = appSettings.Value.FcmNotificationSetting;

            NotificationRepository = notificationRepository;
        }

        public async Task SendNotificationAsync(NotificationSendModel notificationModel)
        {
            try
            {
                using var httpClient = new HttpClient();

                string deviceToken = notificationModel.DeviceId;

                var settings = new FcmSettings
                {
                    SenderId = FcmNotificationSetting.SenderId,
                    ServerKey = FcmNotificationSetting.ServerKey
                };

                string authorizationKey = $"key={settings.ServerKey}";

                httpClient.DefaultRequestHeaders.TryAddWithoutValidation(HeaderNames.Authorization, authorizationKey);

                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MimeTypeNames.ApplicationJson));

                var dataPayload = new DataPayload
                {
                    Title = notificationModel.Title,
                    Body = notificationModel.Body,
                    Id = notificationModel.Id,
                    NotificationType = notificationModel.NotificationType,
                    Badge = notificationModel.Badge
                };

                var notification = new GoogleNotification
                {
                    Data = dataPayload,
                    Notification = dataPayload
                };

                var fcm = new FcmSender(settings, httpClient);

                var fcmSendResponse = await fcm.SendAsync(deviceToken, notification);

                if (!fcmSendResponse.IsSuccess())
                    Log.Error($"Đã có lỗi {fcmSendResponse.Results[0].Error} khi send \n{deviceToken}\n\n");
                else
                    Log.Information($"Đã thành công send \n{deviceToken}\n\n");
            }
            catch (Exception ex)
            {
                Log.Error($"Đã có lỗi {ex.Message} khi send \n{notificationModel.DeviceId}\n\n");
            }
        }

        public async Task<GridResult<NotificationDto>> GetAllPagingAsync(GridParam input)
        {
            var userCode = await GetUserCodeOfCurrentUserAsync();

            var query = NotificationRepository.GetDbSet()
                                              .AsNoTracking()
                                              .Where(x => x.UserCode == userCode)
                                              .OrderByDescending(x => x.CreationTime);

            var totalCount = await query.CountAsync();

            var data = await query.Skip(input.SkipCount).Take(input.MaxResultCount).ToListAsync();

            var notifications = Mapper.Map<List<NotificationDto>>(data);

            foreach (var notification in notifications)
            {
                foreach (var index in notification.ArgumentIndexes)
                {
                    if (index < notification.Arguments.Length)
                        notification.Arguments[index] = L[notification.Arguments[index]];
                }

                notification.Title = L[notification.Title];

                notification.Content = L[notification.Content, notification.Arguments];

                if (notification.NotificationType == NotificationType.Schedule)
                {
                    while (notification.Content.Contains("-  -"))
                    {
                        notification.Content = notification.Content.Replace("-  -", "-");
                    }
                }
            }

            return new GridResult<NotificationDto>(notifications, totalCount);
        }

        public async Task ReadNotificationAsync(int id)
        {
            var userCode = await GetUserCodeOfCurrentUserAsync();

            var notification = await NotificationRepository.GetDbSet()
                                        .FirstOrDefaultAsync(x => x.Id == id && x.UserCode == userCode);

            if (notification == null)
                throw new UserFriendlyException(L["NotificationNotFound"]);

            notification.IsRead = true;

            await NotificationRepository.UpdateAsync(notification, true);
        }

        public async Task<int> GetTotalNotificationUnReadAsync()
        {
            string userCode = await GetUserCodeOfCurrentUserAsync();

            return await NotificationRepository.CountAsync(x => x.UserCode == userCode && !x.IsRead.HasValue);
        }

        public async Task UpdateStatusReadAsync()
        {
            string userCode = await GetUserCodeOfCurrentUserAsync();

            var list = await NotificationRepository.Where(x => x.UserCode == userCode && !x.IsRead.HasValue).ToListAsync();

            foreach (var item in list)
            {
                item.IsRead = false;
            }

            await NotificationRepository.UpdateRangeAsync(list, true);
        }
    }
}
