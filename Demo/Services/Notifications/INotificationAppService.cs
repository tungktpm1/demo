﻿using Demo.Dtos.Notifications;
using Demo.Helpers;

namespace Demo.Services.Notifications
{
    public interface INotificationAppService
    {
        Task SendNotificationAsync(NotificationSendModel notificationModel);

        Task<GridResult<NotificationDto>> GetAllPagingAsync(GridParam input);

        Task ReadNotificationAsync(int id);

        Task<int> GetTotalNotificationUnReadAsync();

        Task UpdateStatusReadAsync();
    }
}
