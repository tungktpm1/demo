﻿using AutoMapper;
using Demo.Constants;
using Demo.Dtos;
using Demo.Dtos.Authentications;
using Demo.Dtos.Users;
using Demo.Entities;
using Demo.ExceptionHandling;
using Demo.Repositories.RefreshTokens;
using Demo.Repositories.Users;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Demo.Services.Authentications
{
    public class AuthenticationAppService : ApplicationServiceBase, IAuthenticationAppService
    {
        private AppSettings AppSettings { get; }

        private IRefreshTokenRepository RefreshTokenRepository { get; }

        private const int ExpriesIn = 86400 * 365 * 2;

        public AuthenticationAppService(
            IConfiguration configuration,
            IMapper mapper,
            IStringLocalizer<ApplicationServiceBase> l,
            IUserRepository userRepository,
            IOptions<AppSettings> appSettings,
            IHttpContextAccessor httpContext,
            IRefreshTokenRepository refreshTokenRepository
        ) : base(configuration, mapper, l, userRepository, httpContext)
        {
            AppSettings = appSettings.Value;

            RefreshTokenRepository = refreshTokenRepository;
        }

        public async Task<AuthenResponse> LoginAsync(AuthenRequest input)
        {
            var user = await UserRepository.GetDbSet()
                                .FirstOrDefaultAsync(x => x.Name == input.UserName && x.Password == input.Password);

            if (user == null)
                throw new UserFriendlyException(L["AccountNotValid"]);

            if (!user.IsActive)
                throw new UserFriendlyException(L["AccountIsLocked"]);

            var userInfo = await UserRepository.GetCurrentUserInfoAsync(user.UserCode);

            var userDto = new UserDto
            {
                Email = user.Email,
                IsAnonymous = userInfo?.IsAnonymous ?? false,
                IsAdmin = userInfo?.IsAdmin ?? false,
                UserCode = user.UserCode,
                Name = user.Name,
                FullName = user.FullName
            };

            var token = GenerateToken(userDto);

            var refreshToken = new RefreshToken
            {
                IsUsed = false,
                UserCode = user.UserCode,
                CreatedDate = DateTime.Now,
                ExpiryDate = DateTime.Now.AddYears(7),
                IsRevoked = false,
                Token = RandomString(25) + Guid.NewGuid()
            };

            await RefreshTokenRepository.AddAsync(refreshToken, true);

            return new AuthenResponse
            {
                AccessToken = token,
                TokenType = JwtBearerDefaults.AuthenticationScheme,
                ExpriesIn = ExpriesIn,
                RefreshToken = refreshToken.Token
            };
        }

        private static string RandomString(int length)
        {
            var random = new Random();

            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            return new string(Enumerable.Repeat(chars, length)
                            .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private string GenerateToken(UserDto user)
        {
            var claims = new[]
            {
                new Claim(ClaimTypeConst.UserCode, user.UserCode),
                new Claim(ClaimTypeConst.Name, user.Name),
                new Claim(ClaimTypeConst.FullName, user.FullName),
                new Claim(ClaimTypeConst.Role, user.IsAdmin ? "admin" : "user"),
                new Claim(ClaimTypeConst.Anonymous, user.IsAnonymous.ToString())
            };

            return GenerateTokenByClaim(claims);
        }

        private string GenerateTokenByClaim(IEnumerable<Claim> claims)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]));

            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(Configuration["Jwt:Issuer"],
                Configuration["Jwt:Audience"],
                claims,
                expires: DateTime.Now.AddSeconds(ExpriesIn),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public async Task<AuthenResponse> RefreshTokenAsync(RefreshTokenRequest input)
        {
            var refreshToken = await RefreshTokenRepository.GetDbSet()
                                            .FirstOrDefaultAsync(x => x.Token == input.RefreshToken);

            if (refreshToken == null)
                throw new UserFriendlyException(L["RefreshTokenInvalid"]);

            if (refreshToken.IsUsed)
                throw new UserFriendlyException(L["RefreshTokenUsed"]);

            if (refreshToken.IsRevoked)
                throw new UserFriendlyException(L["RefreshTokenRevoked"]);

            if (refreshToken.ExpiryDate < DateTime.Now)
                throw new UserFriendlyException(L["RefreshTokenExpries"]);

            refreshToken.IsUsed = true;

            await RefreshTokenRepository.UpdateAsync(refreshToken);

            var user = await UserRepository.GetUserByEmailAsync(refreshToken.UserCode);

            if (user == null)
                throw new UserFriendlyException(L["AccountNotValid"]);

            if (!user.IsActive)
                throw new UserFriendlyException(L["AccountIsLocked"]);

            var accessToken = GenerateToken(user);

            var refreshTokenNew = new RefreshToken
            {
                IsUsed = false,
                UserCode = user.UserCode,
                CreatedDate = DateTime.Now,
                ExpiryDate = DateTime.Now.AddYears(7),
                IsRevoked = false,
                Token = RandomString(25) + Guid.NewGuid()
            };

            await RefreshTokenRepository.AddAsync(refreshTokenNew, true);

            return new AuthenResponse
            {
                AccessToken = accessToken,
                TokenType = JwtBearerDefaults.AuthenticationScheme,
                ExpriesIn = ExpriesIn,
                RefreshToken = refreshTokenNew.Token
            };
        }
    }
}
