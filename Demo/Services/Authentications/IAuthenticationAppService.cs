﻿using Demo.Dtos.Authentications;

namespace Demo.Services.Authentications
{
    public interface IAuthenticationAppService
    {
        Task<AuthenResponse> LoginAsync(AuthenRequest input);

        Task<AuthenResponse> RefreshTokenAsync(RefreshTokenRequest input);
    }
}
