﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Demo.Attributes;
using Demo.Dtos.Notifications;
using Demo.Helpers;
using Demo.Services.Notifications;

namespace Demo.Controllers
{
    [Route("api/notification")]
    public class NotificationController : AppBaseController, INotificationAppService
    {
        private INotificationAppService NotificationAppService { get; }

        public NotificationController(INotificationAppService notificationAppService)
        {
            NotificationAppService = notificationAppService;
        }

        [HttpPost]
        [Route("send")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task SendNotificationAsync(NotificationSendModel notificationModel)
        {
            await NotificationAppService.SendNotificationAsync(notificationModel);
        }

        [HttpPost]
        [Route("paging")]
        [CustomAuthorize]
        [AnonymousVote]
        public async Task<GridResult<NotificationDto>> GetAllPagingAsync(GridParam input)
        {
            return await NotificationAppService.GetAllPagingAsync(input);
        }

        [HttpPost]
        [Route("read/{id}")]
        [CustomAuthorize]
        [AnonymousVote]
        public async Task ReadNotificationAsync(int id)
        {
            await NotificationAppService.ReadNotificationAsync(id);
        }

        [HttpGet]
        [Route("total-unread")]
        [CustomAuthorize]
        [AnonymousVote]
        public async Task<int> GetTotalNotificationUnReadAsync()
        {
            return await NotificationAppService.GetTotalNotificationUnReadAsync();
        }

        [HttpPost]
        [Route("update-notification-status")]
        [CustomAuthorize]
        [AnonymousVote]
        public async Task UpdateStatusReadAsync()
        {
            await NotificationAppService.UpdateStatusReadAsync();
        }
    }
}
