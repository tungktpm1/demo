﻿using Microsoft.AspNetCore.Mvc;
using Demo.Attributes;
using Demo.Constants;
using Demo.Dtos.Users;
using Demo.Helpers;
using Demo.Services.Users;
using System.Net.Mime;
using Deno.Dtos.Users;

namespace Demo.Controllers
{
    [Route("api/user")]
    public class UserController : AppBaseController, IUserAppService
    {
        private IUserAppService UserAppService { get; }

        public UserController(IUserAppService userAppService)
        {
            UserAppService = userAppService;
        }

        [HttpGet]
        [Route("current")]
        [CustomAuthorize]
        [AnonymousVote]
        public async Task<UserDto> GetCurrentUserLoginAsync()
        {
            return await UserAppService.GetCurrentUserLoginAsync();
        }

        [HttpGet]
        [Route("all")]
        [CustomAuthorize]
        public async Task<List<UserDto>> GetAllUsersAsync()
        {
            return await UserAppService.GetAllUsersAsync();
        }

        [HttpPost]
        [Route("update-current-user")]
        [CustomAuthorize]
        [AnonymousVote]
        public async Task UpdateAsync([FromForm] UserUpdateRequest input)
        {
            await UserAppService.UpdateAsync(input);
        }

        [HttpPost]
        [Route("change-password")]
        [CustomAuthorize]
        public async Task ChangePasswordAsync(ChangePasswordContent input)
        {
            await UserAppService.ChangePasswordAsync(input);
        }

        [HttpGet]
        [Route("get-user-by-code")]
        public async Task<UserDto> GetUserByCodeAsync(string code)
        {
            return await UserAppService.GetUserByCodeAsync(code);
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public Task<byte[]> DownloadQrCodeAsync()
        {
            throw new NotImplementedException();
        }
    }
}
