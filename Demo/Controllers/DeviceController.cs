﻿using Microsoft.AspNetCore.Mvc;
using Demo.Attributes;
using Demo.Dtos.Devices;
using Demo.Services.Devices;

namespace Demo.Controllers
{
    [CustomAuthorize]
    [Route("api/device")]
    public class DeviceController : AppBaseController, IDeviceAppService
    {
        private IDeviceAppService DeviceAppService { get; }

        public DeviceController(IDeviceAppService deviceAppService)
        {
            DeviceAppService = deviceAppService;
        }

        [HttpPost]
        [Route("add")]
        public async Task AddAsync(DeviceRequest input)
        {
            await DeviceAppService.AddAsync(input);
        }

        [HttpPost]
        [Route("delete")]
        public async Task DeleteAsync(DeviceRequest input)
        {
            await DeviceAppService?.DeleteAsync(input);
        }
    }
}
