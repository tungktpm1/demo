﻿using Demo.Dtos.Authentications;
using Demo.Services.Authentications;
using Microsoft.AspNetCore.Mvc;

namespace Demo.Controllers
{
    [Route("login")]
    public class AuthenticationController : AppBaseController, IAuthenticationAppService
    {
        private IAuthenticationAppService AuthenticationAppService { get; }

        public AuthenticationController(IAuthenticationAppService authenticationAppService)
        {
            AuthenticationAppService = authenticationAppService;
        }

        [HttpPost]
        [Route("token")]
        public async Task<AuthenResponse> LoginAsync([FromForm] AuthenRequest input)
        {
            return await AuthenticationAppService.LoginAsync(input);
        }

        [HttpPost]
        [Route("refresh-token")]
        public async Task<AuthenResponse> RefreshTokenAsync(RefreshTokenRequest input)
        {
            return await AuthenticationAppService.RefreshTokenAsync(input);
        }
    }
}
