﻿using Application.Hubs;
using Demo.Dtos;
using Demo.EntityFrameworkCore;
using Demo.Middlewares;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;
using Microsoft.OpenApi.Models;
using System.Globalization;
using System.Net.Mime;

namespace Demo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                .ConfigureApiBehaviorOptions(options =>
                {
                    options.InvalidModelStateResponseFactory = context =>
                        new BadRequestObjectResult(context.ModelState)
                        {
                            ContentTypes =
                            {
                                MediaTypeNames.Application.Json,
                                MediaTypeNames.Application.Xml
                            }
                        };
                }).AddXmlSerializerFormatters();

            services.AddEndpointsApiExplorer();

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "Demo API", Version = "v1" });
                options.CustomSchemaIds(type => type.FullName);
                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = HeaderNames.Authorization,
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.Http,
                    Scheme = JwtBearerDefaults.AuthenticationScheme
                });

                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new List<string>()
                    }
                });
            });

            services.AddDbContext<DemoDbContext>(
                options => options.UseSqlServer(Configuration.GetConnectionString("Default")),
                ServiceLifetime.Scoped
            );

            services.AddLocalization();

            services.AddSignalR();

            services.AddDistributedMemoryCache();

            services.Configure<AppSettings>(Configuration.GetSection("Config"));

            services.AddMemoryCache();

            services.RegisterServices();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseSwagger();

                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Demo v1"));
            }

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            var options = new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture(new CultureInfo("vi"))
            };

            app.UseStaticFiles();

            app.UseMiddleware<LocalizationMiddleware>();

            app.UseMiddleware<AutoLogRequestMiddleware>();

            app.UseMiddleware<DbTransactionMiddleware>();

            app.UseMiddleware<CustomAuthorizeMiddleware>();

            app.UseMiddleware<AdminFilterMiddleware>();

            app.UseMiddleware<AnonymousVoteFilterMiddleware>();

            app.UseMiddleware<HandleExceptionMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<ChatHub>($"/{nameof(ChatHub)}");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
