﻿namespace Demo.Constants.DemoApiStatusConsts
{
    public static class DemoApiStatusConst
    {
        public const string SuccessCode = "00";
        public const string DataInvalid = "-404";
        public const string ExceptionError = "-1";
        public const string DataError = "-400";
        public const string SignInvalid = "-105";
    }
}
