﻿namespace Demo.Constants.EntityConsts
{
    public static class UserConst
    {
        public const int UserCodeLength = 10;
        public const int NameLength = 50;
        public const int FullNameLength = 50;
        public const int PasswordLength = 250;
        public const int GroupCodeLength = 10;
        public const int DepartmentCodeLength = 8;
        public const int DivisionCodeLength = 11;
        public const int HospitalCodeLength = 20;      
        public const int StatusCodeLength = 2;
        public const int MaCtuArrLength = 150;
        public const int MaPktArrLength = 150;
        public const int MaPttaiArrLength = 150;
        public const int PaymentStatusCodeLength = 8;
        public const int NoteLength = 500;
        public const int BankCodeLength = 11;
        public const int EmailLength = 50;
        public const int PositionCodeLength = 2;
        public const int EmployeeCodeLength = 11;
        public const int ParentIdLength = 10;
        public const int OtpCodeLength = 6;
        public const int ChannelTypeLength = 10;
        public const int AvatarLength = 500;
        public const int PhoneNumberLength = 20;
        public const int MaKHLength = 50;
        public const int PasswordSignLength = 50;
        public const int PasswordSha256Length = 250;
    }
}
