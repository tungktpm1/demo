﻿namespace Demo.Constants
{
    public static class ClaimTypeConst
    {
        public const string UserCode = "usercode";
        public const string Name = "name";
        public const string FullName = "fullname";
        public const string Role = "role";
        public const string Anonymous = "anonymous";
    }
}
