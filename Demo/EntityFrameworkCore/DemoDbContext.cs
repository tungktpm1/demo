﻿using Demo.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Demo.EntityFrameworkCore
{
    public class DemoDbContext : DbContext
    {
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<RefreshToken> RefreshTokens { get; set; }

        public virtual DbSet<Device> Devices { get; set; }

        public virtual DbSet<UserInfo> UserInfos { get; set; }

        public virtual DbSet<Notification> Notifications { get; set; }

        public DemoDbContext(DbContextOptions<DemoDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Notification>(b =>
            {
                const string join = "; ";

                var comparer = new ValueComparer<string[]>(
                   (l, r) => string.Join(join, l) == string.Join(join, r),
                   v => v.Length == 0 ? 0 : string.Join(join, v).GetHashCode(),
                   v => v);

                b.Property(r => r.Arguments)
                    .HasConversion(
                        v => string.Join(join, v),
                        v => !string.IsNullOrEmpty(v) ? v.Split(join, StringSplitOptions.None) : Array.Empty<string>())
                    .Metadata.SetValueComparer(comparer);

                b.Property(x => x.Arguments).IsRequired();

                var compareInt = new ValueComparer<int[]>(
                   (l, r) => string.Join(join, l) == string.Join(join, r),
                   v => v.Length == 0 ? 0 : string.Join(join, v).GetHashCode(),
                   v => v);

                b.Property(r => r.ArgumentIndexes)
                    .HasConversion(
                        v => string.Join(join, v),
                        v => v.Split(join, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToArray())
                    .Metadata.SetValueComparer(compareInt);

                b.Property(x => x.ArgumentIndexes).IsRequired();
            });

            modelBuilder.Entity<Device>(b =>
            {
                b.Property(x => x.Language).IsRequired().HasDefaultValue("vi");
            });
        }
    }
}
