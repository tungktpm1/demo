﻿using Microsoft.AspNetCore.Http.Features;
using Demo.Attributes;
using Demo.Extensions;
using Demo.Repositories.Users;
using Serilog;
using System.Net;

namespace Demo.Middlewares
{
    public class AnonymousVoteFilterMiddleware
    {
        private RequestDelegate Next { get; }

        private IServiceProvider ServiceProvider { get; }

        public AnonymousVoteFilterMiddleware(RequestDelegate next, IServiceProvider serviceProvider)
        {
            Next = next;
            ServiceProvider = serviceProvider;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var endpoint = httpContext.Features.Get<IEndpointFeature>()?.Endpoint;

            var attribute = endpoint?.Metadata.GetMetadata<AnonymousVoteAttribute>();

            try
            {
                using var scope = ServiceProvider.CreateScope();

                var userRepository = scope.ServiceProvider.GetRequiredService<IUserRepository>();

                string userName = httpContext.GetEmailOfUserLogin();

                var user = await userRepository.GetUserByEmailAsync(userName);

                bool isAnonymousVote = user?.IsAnonymous ?? false;

                if (isAnonymousVote && attribute == null)
                {
                    httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;

                    return;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);

                Log.Error(ex.StackTrace);

                Log.Error(ex.InnerException.Message);

                httpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                return;
            }

            await Next(httpContext);
        }
    }
}
