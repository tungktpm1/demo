﻿using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Localization;
using Demo.Attributes;
using Demo.EntityFrameworkCore;
using Demo.ExceptionHandling;
using Serilog;

namespace Demo.Middlewares
{
    public class DbTransactionMiddleware
    {
        private RequestDelegate Next { get; }

        private IStringLocalizer<DbTransactionMiddleware> L;

        public DbTransactionMiddleware(RequestDelegate next, IStringLocalizer<DbTransactionMiddleware> l)
        {
            Next = next;

            L = l;
        }

        public async Task Invoke(HttpContext httpContext, DemoDbContext context)
        {
            if (httpContext.Request.Method.Equals("GET", StringComparison.CurrentCultureIgnoreCase))
            {
                await Next(httpContext);
                return;
            }

            // If action is not decorated with TransactionAttribute then skip opening transaction
            var endpoint = httpContext.Features.Get<IEndpointFeature>()?.Endpoint;
            var attribute = endpoint?.Metadata.GetMetadata<TransactionAttribute>();
            if (attribute == null)
            {
                await Next(httpContext);
                return;
            }

            await using var transaction = await context.Database.BeginTransactionAsync();

            try
            {
                await Next(httpContext);

                if (httpContext.Response.StatusCode is StatusCodes.Status403Forbidden or StatusCodes.Status500InternalServerError)
                {
                    await transaction.RollbackAsync();
                }
                else
                {
                    await context.SaveChangesAsync();

                    await transaction.CommitAsync();
                }
            }
            catch (UserFriendlyException ex)
            {
                await transaction.RollbackAsync();
            }
            catch (Exception ex)
            {
                await transaction.RollbackAsync();

                Log.Error(ex.Message);

                Log.Error(ex.StackTrace);

                throw new UserFriendlyException(L["InternalServerError"]);
            }
        }
    }
}
