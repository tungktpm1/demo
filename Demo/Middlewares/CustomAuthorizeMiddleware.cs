﻿using Demo.Attributes;
using Demo.Dtos;
using Demo.Dtos.Authentications;
using Demo.Extensions;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using Serilog;
using System.Net;
using System.Web;

namespace Demo.Middlewares
{
    public class CustomAuthorizeMiddleware
    {
        private RequestDelegate Next { get; }

        private AppSettings AppSettings { get; }

        public CustomAuthorizeMiddleware(RequestDelegate next, IOptions<AppSettings> options)
        {
            Next = next;

            AppSettings = options.Value;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var endpoint = httpContext.Features.Get<IEndpointFeature>()?.Endpoint;

            var attribute = endpoint?.Metadata.GetMetadata<CustomAuthorizeAttribute>();

            if (attribute != null)
            {
                try
                {
                    bool isValidToken = httpContext.Request.Headers.TryGetValue(HeaderNames.Authorization, out var tokenString);

                    string token = tokenString;

                    if (!isValidToken || !token.HasValue())
                    {
                        Log.Error($"Validate token fail: Token không hợp lệ");

                        httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;

                        return;
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);

                    Log.Error(ex.StackTrace);

                    Log.Error(ex.InnerException.Message);

                    httpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                    return;
                }
            }

            await Next(httpContext);
        }
    }
}
