﻿using Microsoft.AspNetCore.Http.Features;
using Demo.Attributes;
using Demo.Extensions;
using Demo.Repositories.Users;
using System.Net;

namespace Demo.Middlewares
{
    public class AdminFilterMiddleware
    {
        private RequestDelegate Next { get; }

        private IServiceProvider ServiceProvider { get; }

        public AdminFilterMiddleware(RequestDelegate next, IServiceProvider serviceProvider)
        {
            Next = next;
            ServiceProvider = serviceProvider;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var endpoint = httpContext.Features.Get<IEndpointFeature>()?.Endpoint;

            var attribute = endpoint?.Metadata.GetMetadata<AdminAttribute>();

            if (attribute != null)
            {
                using var scope = ServiceProvider.CreateScope();

                var userRepository = scope.ServiceProvider.GetRequiredService<IUserRepository>();

                string email = httpContext.GetEmailOfUserLogin();

                var user = await userRepository.GetUserByEmailAsync(email);

                bool isadmin = user?.IsAdmin ?? false;

                if (!isadmin)
                {
                    httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;

                    return;
                }
            }
            await Next(httpContext);
        }
    }
}
